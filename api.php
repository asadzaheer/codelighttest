<?php
$cards = [
    [
        'number' => 37250123123,
        'blocked' => false,
        'balance' => 0.01,
        'curr' => 'USD'
    ],
    [
        'number' => 37250123124,
        'blocked' => false,
        'balance' => 72.00,
        'curr' => 'EUR'
    ],
    [
        'number' => 37250123125,
        'blocked' => true,
        'balance' => 72.00,
        'curr' => 'USD'
    ],
    [
        'number' => 37250123126,
        'blocked' => false,
        'balance' => 72.00,
        'curr' => 'USD'
    ],
];

// Define getallheaders in case of nginx server.
if (!function_exists('getallheaders'))
{
    function getallheaders() {
        $headers = [];
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

$headers = getallheaders();
if (isset($headers['Authorization']))
{
    $authType        = explode(' ', $headers['Authorization'])[0];
    $authCredentials = explode(' ', $headers['Authorization'])[1];
    $authUser        = explode(':', base64_decode($authCredentials))[0];
    $authPassword    = explode(':', base64_decode($authCredentials))[1];
    if ($authUser === 'codelight' && $authPassword === '123456')
    {
        if ($_GET)
        {
            if (isset($_GET['action']))
            {
                if ($_GET['action'] === 'addBalance')
                {
                    unset($_GET['action']);
                    if (isset($_GET['currency']) && isset($_GET['amount']) && isset($_GET['number']))
                    {
                        addBalance($_GET);
                    }
                    else
                    {
                        getResponse('results', 'ERROR', 'Missing parameters.');
                    }
                }
                else if ($_GET['action'] === 'getBalance')
                {
                    unset($_GET['action']);
                    if (isset($_GET['number']))
                    {
                        getBalance($_GET);
                    }
                    else
                    {
                        getResponse('results', 'ERROR', 'Card number not provided.');
                    }
                }
                else
                {
                    getResponse('results', 'ERROR', 'Invalid action parameter.');
                }
            }
            else
            {
                getResponse('results', 'ERROR', 'Action parameter missing.');
            }
        }
        else
        {
            getResponse('results', 'ERROR', 'No data provided.');
        }
    }
    else
    {
        getResponse('results', 'ERROR', 'Authorization failed.');
    }
}
else
{
    getResponse('results', 'ERROR', 'No Authorization provided.');
}

function addBalance($data) {
    global $cards;

    $key = array_search($data['number'], array_column($cards, 'number'));

    if ($key !== FALSE)
    {
        if (!$cards[$key]['blocked'])
        {
            if ($cards[$key]['curr'] == $data['currency'])
            {
                if ($cards[$key]['balance'] <= -0.01 && $cards[$key]['balance'] >= -4.99)
                {
                    $data['amount']         -= $cards[$key]['balance'];
                    $cards[$key]['balance'] = number_format($cards[$key]['balance'] + $data['amount'], 2);
                    $xml = new SimpleXMLElement('<xml/>');

                    $results = $xml->addChild('addBalance');
                    $results->addChild('balance', $cards[$key]['balance']);

                    Header('Content-type: text/xml');
                    echo($xml->asXML());
                }
                else if ($cards[$key]['balance'] <= -5)
                {
                    getResponse('addBalance', 'ERROR', 'Failed to add balance, syntax error.');
                }
                else
                {
                    $cards[$key]['balance'] = number_format($cards[$key]['balance'] + $data['amount'], 2);
                    $xml = new SimpleXMLElement('<xml/>');

                    $results = $xml->addChild('addBalance');
                    $results->addChild('balance', $cards[$key]['balance']);

                    Header('Content-type: text/xml');
                    echo($xml->asXML());
                }
            }
            else
            {
                getResponse('addBalance', 'ERROR', 'Provided currency (' . $data['currency'] . ') is not same as card currency (' . $cards[$key]['curr'] . ').');
            }
        }
        else
        {
            getResponse('addBalance', 'ERROR', 'Card is blocked.');
        }
    }
    else
    {
        getResponse('addBalance', 'ERROR', 'Card not found.');
    }
}

function getBalance($data) {
    global $cards;

    $key = array_search($data['number'], array_column($cards, 'number'));

    if ($key !== FALSE)
    {
        $xml = new SimpleXMLElement('<xml/>');

        $results = $xml->addChild('results');

        $card = $results->addChild('card');
        $card->addChild('number', $cards[$key]['number']);
        $card->addChild('blocked', ($cards[$key]['blocked']) ? 'TRUE' : 'FALSE');
        $card->addChild('balance', $cards[$key]['balance']);
        $card->addChild('curr', $cards[$key]['curr']);

        Header('Content-type: text/xml');
        echo($xml->asXML());
    }
    else
    {
        getResponse('results', 'ERROR', 'Card not found.');
    }
}

function getResponse($parentName, $type, $text) {
    $xml = new SimpleXMLElement('<xml/>');

    $results = $xml->addChild($parentName);

    $results->addChild('type', $type);
    $results->addChild('text', $text);

    Header('Content-type: text/xml');
    echo ($xml->asXML());
}