<?php

class TopUpHandler {
    var $config = [
        'notificationMethod' => 'email',
        'notificationEmail' => 'somebody@example.com',
        'apiUrl' => 'http://localhost/codelighttest/api.php',
        'apiUser' => '',
        'apiPass' => ''
    ];

    function __construct() {

    }

    function addBalance($number, $currency, $amount) {
        $amount = $amount / 100; // Convert cents to euro

        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FAILONERROR => 1,
            CURLOPT_URL => $this->config['apiUrl'] . '?action=addBalance&number=' . $number . '&currency=' . $currency . '&amount=' . $amount,
            CURLOPT_HTTPHEADER => array('Authorization: Basic ' . base64_encode($this->config['apiUser'] . ':' . $this->config['apiPass'])),
            CURLOPT_TIMEOUT => 60
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Check if timeout/404/500 error occurred
        if (curl_errno($curl) === 28 || curl_errno($curl) === 22)
        {
            $this->sendNotification('Failed to add balance to user card', 'User failed to add balance for Number : ' . $number . ', Amount : ' . $amount . ' due to server failure, Error details : ' . curl_error($curl));
            $xml = new SimpleXMLElement('<xml/>');

            $results = $xml->addChild('results');

            $results->addChild('type', 'ERROR');
            $results->addChild('text', 'Server error.');

            Header('Content-type: text/xml');
            return ($xml->asXML());
        }

        // Close request to clear up some resources
        curl_close($curl);

        return $resp;
    }

    function getBalance($number) {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FAILONERROR => 1,
            CURLOPT_URL => $this->config['apiUrl'] . '?action=getBalance&number=' . $number,
            CURLOPT_HTTPHEADER => array('Authorization: Basic ' . base64_encode($this->config['apiUser'] . ':' . $this->config['apiPass'])),
            CURLOPT_TIMEOUT => 1
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);

        // Check if timeout/404/500 error occurred
        if (curl_errno($curl) === 28 || curl_errno($curl) === 22)
        {
            $this->sendNotification('Failed to get user card balance', 'User failed to get balance for : ' . $number . ' due to server failure, Error details : ' . curl_error($curl));
            $xml = new SimpleXMLElement('<xml/>');

            $results = $xml->addChild('results');

            $results->addChild('type', 'ERROR');
            $results->addChild('text', 'Server error.');

            Header('Content-type: text/xml');
            return ($xml->asXML());
        }

        // Close request to clear up some resources
        curl_close($curl);

        return $resp;
    }

    function sendNotification($subject, $txt) {
        if ($this->config['notificationMethod'] == 'email')
        {
            $to = $this->config['notificationEmail'];

            mail($to, $subject, $txt);
        }
    }
}
