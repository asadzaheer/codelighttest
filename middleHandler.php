<?php
include("topUpHandler.php");
header('Content-type: application/xml');

$topUpHandler = new TopUpHandler();

$topUpHandler->config['notificationMethod'] = 'email'; // Set notification method Email/SMS by default set to email
$topUpHandler->config['notificationEmail']  = 'somebody@example.com'; // Set notification email to whom notification will be sent
$topUpHandler->config['apiUrl']             = 'http://localhost/codelighttest/api.php'; // Set api user
$topUpHandler->config['apiUser']            = 'codelight'; // Set api user
$topUpHandler->config['apiPass']            = '123456'; // Set api password

if ($_GET)
{
    if (isset($_GET['action']))
    {
        if ($_GET['action'] == 'getBalance')
        {
            $result = $topUpHandler->getBalance($_GET['number']);
        }
        else if ($_GET['action'] == 'addBalance')
        {
            $result = $topUpHandler->addBalance($_GET['number'], $_GET['currency'], $_GET['amount'] * 100);
        }
        else
        {
            $xml = new SimpleXMLElement('<xml/>');

            $results = $xml->addChild('results');

            $results->addChild('type', 'ERROR');
            $results->addChild('text', 'Invalid action provided.');

            $result = ($xml->asXML());
        }
    }
    else
    {
        $xml = new SimpleXMLElement('<xml/>');

        $results = $xml->addChild('results');

        $results->addChild('type', 'ERROR');
        $results->addChild('text', 'No action provided.');

        $result = ($xml->asXML());
    }
}
else
{
    $xml = new SimpleXMLElement('<xml/>');

    $results = $xml->addChild('results');

    $results->addChild('type', 'ERROR');
    $results->addChild('text', 'No data provided.');

    $result = ($xml->asXML());
}


echo $result;