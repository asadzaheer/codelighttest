Codelight Sim card top-up.

This Repo includes following files.

api.php - Is a mock api for this test.

topUpHandler.php - It contain main functionality of this api, addBalance / getBalance / senNotifications.
 
middleHandler.php - It handle communication between index.php and api.php through topUpHandler.php.

index.php - Contains simple interface to add or check sim card balance.


To use top-up functionality simply include topUpHandler.php file, you can configure its
following options.

notificationMethod - Defines which method for notification should be used (email/sms).

notificationEmail - Defines the email address to whom the notification email will be sent.

apiUrl - Define api URL.

apiUser - Define username for authorization api (use 'codelight' for this test).

apiPass - Define password used for authorization of api (use '123456' for this test).

Example to set notification method.

    $topUpHandler = new TopUpHandler();
    
    $topUpHandler->config['notificationMethod'] = 'email';
    
Example using function addBalance function
    
    $topUpHandler = new TopUpHandler();
    
    $topUpHandler->addBalance(3725123123, 'EUR', 1000);
    

Test Cases are included in file : Codelight Test Cases.docx