<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Codelight Sim Cards Top-up</title>
    <meta name="description" content="Codelight Sim Cards Top-up Test">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body class="text-center">
<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-sm">
            <div class="result row justify-content-sm-center">

            </div>
            <div class="welcome row justify-content-sm-center">
                <h2 class="col-12">Welcome please select an option</h2>
                <div class="col-12">
                    <button class="btn btn-primary" id="addBalance">Add Balance</button>
                    <button class="btn btn-primary" id="getBalance">Check Balance</button>
                </div>
            </div>
            <div class="addBalanceDiv hide row justify-content-sm-center">
                <h2 class="col-12">Enter following details.</h2>
                <div class="col-6">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="currency">
                                Currency
                            </label>
                            <select class="form-control" name="currency" id="currency">
                                <option value="EUR">Euro</option>
                                <option value="USD">USD</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="amount">
                                Amount
                            </label>
                            <input class="form-control" type="number" id="amount" min="0.00" name="amount">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="number">
                                Number
                            </label>
                            <input class="form-control" type="number" id="number" name="number">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <button class="goBack btn btn-danger">Go Back</button>
                    <button class="proceed btn btn-primary" data-action="addBalance">Proceed</button>
                </div>
            </div>
            <div class="getBalanceDiv hide row justify-content-sm-center">
                <h2 class="col-12">Enter your sim card number below.</h2>
                <div class="col-6">
                    <div class="form-group">
                        <label for="number">
                            number
                        </label>
                        <input class="form-control" type="number" name="number" id="number">
                    </div>
                </div>
                <div class="col-12">
                    <button class="goBack btn btn-danger">Go Back</button>
                    <button class="proceed btn btn-primary" data-action="getBalance">Proceed</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#addBalance').on('click', function () {
            $('.addBalanceDiv').removeClass('hide');
            $('.welcome').addClass('hide');
        });

        $('#getBalance').on('click', function () {
            $('.getBalanceDiv').removeClass('hide');
            $('.welcome').addClass('hide');
        });

        $('.goBack').on('click', function () {
            $('.result').html('');
            $('.welcome').removeClass('hide');
            $('.addBalanceDiv').addClass('hide');
            $('.getBalanceDiv').addClass('hide');
        });

        $('.proceed').on('click', function () {
            var action = $(this).data('action'), username = 'codelight', password = '123456',
                encoded = btoa(username + ':' + password), data = {};

            if (action === 'addBalance') {
                data = {
                    action: 'addBalance',
                    currency: $('.addBalanceDiv select[name=currency]').val(),
                    amount: parseFloat($('.addBalanceDiv input[name=amount]').val()).toFixed(2),
                    number: $('.addBalanceDiv input[name=number]').val()
                };
            }

            if (action === 'getBalance') {
                data = {
                    action: 'getBalance',
                    number: $('.getBalanceDiv input[name=number]').val()
                };
            }

            $.ajax({
                type: 'GET',
                dataType: 'xml',
                /*beforeSend: function (request) {
                 request.setRequestHeader("Authorization", 'Basic ' + encoded);
                 },*/
                url: 'middleHandler.php',
                data: data
            }).done(function (data) {
                var html = '';
                if ($(data).find('type').text() !== 'ERROR') {
                    if ($(data).find('blocked').text() != "TRUE") {
                        if (action === 'addBalance') {
                            html = "<div class='col-6'><div class='alert alert-success'>Balance added to card new balance : " + $(data).find('addBalance balance').text() + "</div></div>";
                            $('.result').html(html);
                        }
                        if (action === 'getBalance') {
                            html = "<div class='col-6'><div class='alert alert-success'><div class='col-12'>Number : " + $(data).find('card number').text() + "</div><div class='col-12'>Balance : " + $(data).find('card balance').text() + "</div><div class='col-12'>Currency : " + $(data).find('card curr').text() + "</div></div></div>";
                            $('.result').html(html);
                        }
                    } else {
                        html = "<div class='col-6'><div class='alert alert-danger'>This card is blocked.</div></div>";
                        $('.result').html(html);
                    }
                } else {
                    html = "<div class='col-6'><div class='alert alert-danger'>" + $(data).find('text').text() + "</div></div>";
                    $('.result').html(html);
                }
            });
        });

    });
</script>
</body>
</html>